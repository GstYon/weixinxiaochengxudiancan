/*
  所有事件放在  onload 或者 onready中
  //添加监听事件
  app.event.on('test2', this, function ( ret ) {
    //ret里面是 send传递过来的参数 
    console.log(ret);
  })
  //发送监听
  app.event.send('test2', {"Log-Page-Btn-Press":1})
  //移除事件
  app.event.remove('test2', this)
*/

var events = {};

function on(name, self, callback) {
  var tuple = [self, callback];
  var callbacks = events[name];
  if (Array.isArray(callbacks)) {
    callbacks.push(tuple);
  }
  else {
    events[name] = [tuple];
  }
}

function remove(name, self) {
  var callbacks = events[name];
  if (Array.isArray(callbacks)) {
    events[name] = callbacks.filter((tuple) => {
      return tuple[0] != self;
    })
  }
}

function send(name, data) {
  if( typeof data =='undefined'){
    data = {}
  }

  var callbacks = events[name];
  if (Array.isArray(callbacks)) {
    callbacks.map((tuple) => {
      var self = tuple[0];
      var callback = tuple[1];
      callback.call(self, data);
    })
  }
}

exports.on = on;
exports.remove = remove;
exports.send = send;