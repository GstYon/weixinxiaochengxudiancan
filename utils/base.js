/* 
 +-----------------------------------------------
   快速开发函数封装
 +-----------------------------------------------
   2206/05/25
 +-----------------------------------------------
 */
//封装常用方法

var event = require('./events.js');

class Base {

  constructor() {
    "use strict";

    this.Conf = {
      //服务端设置
      serverUrl: 'https://www.xx.com/',   //接口访问地址   //
     
      debug: false,           //开启调试模式  true 显示调试日志console.log false不显示

      //_toast 延迟时长
      delay: 2000,
      duration: 2000,       //持续时长，单位：毫秒
      location: 'bottom',   //弹出位置，顶部、中间或底部
      version:'1.0.4',
      //ajax header
      request_header:{
        'Content-Type': 'application/json' // 默认值
      }
    }

  }
	
	/**
	 * ajax请求
	 * @param string     url      ajax请求地址
	 * @param array      data     请求数据 json对象  {'1':1,'2':2}
	 * @param string    _method   请求类型 默认 post
	 * @param function  _call     请求成功回调函数
	 * @param string    _datatype 回调类型 默认 json
	 * @param filestr   _files    文件路径  {'file1':file}
	 */
	_ajax(url,data,_method,_call,_datatype){

     
    var that = this;
		var _data = ( typeof arguments[1] == "undefined" || arguments[1] == null) ? {}  : arguments[1];
		var _method = ( typeof arguments[2] == "undefined" || arguments[2] == null) ? "post"  : arguments[2];
		var _datatype = ( typeof arguments[4] == "undefined" || arguments[4] == null) ? "json"  : arguments[4];
		//判断是wx还是H5
    var is_api = this._isCloud();

   
		if(is_api =='wx'){
      // _data.token = '286c4b2831a3aeaa867c02a1ce214362';
      wx.request({
        url:  url, //仅为示例，并非真实的接口地址
        method: _method,
        dataType: _datatype,
        data: _data,
        header: this.Conf.request_header,
        success(res) {
          console.log(res);
          if (that.Conf.debug) {
            console.log("返回内容：" + JSON.stringify(res));
          }

          //判断异常状态
          console.log(res.data.errcode);
          if( res.data.errcode ==110){
            that._toast('登录状态失效，请重新授权');
            //发送监听信息
            event.send('userInfo', {});

            wx.removeStorageSync('userInfo');
            
          }

          if (typeof _call == "function") {
            _call(res.data);
          }
        },
        fail(err){
          console.log("返回内容err：" + JSON.stringify(err));
          that._hideLoading();
          
        }
      });

		}else{
			$.ajax({
				url: url,
				data: _data,
				method:_method,
				dataType: "json",
				success: function(ret){
          console.log(ret);
          
          
					if ( typeof _call == "function") {
						_call(ret);
					}
				},
				error:function(){
					alert("网络连接失败，请稍后再试");
					return false;
				}
			});
		}
	}

  _toast(msg, callback, delay){
    var delay = (typeof arguments[2] == "undefined" || arguments[2] == null) ? this.Conf.delay : arguments[2];  //延迟时长

    var is_api = this._isCloud();

    if (is_api == 'wx') {
      wx.showToast({
        title: msg,
        icon: 'none'
      });

      if (typeof callback == "function") {
        setTimeout(function () {
          callback();
        }, delay);
      }

    } else{

    }
  }
  
  //显示加载
  _showLoading(msg, callback, delay) {
    var delay = (typeof arguments[2] == "undefined" || arguments[2] == null) ? this.Conf.delay : arguments[2];  //延迟时长

    var is_api = this._isCloud();

    if (is_api == 'wx') {
      wx.showLoading({
        title: msg,
        mask: true,
      })

      if (typeof callback == "function") {
        setTimeout(function () {
          callback();
        }, delay);
      }

    } else {

    }
  }

  _hideLoading(){
    var is_api = this._isCloud();

    if (is_api == 'wx') {
      wx.hideLoading()
    } 
  }

  //跳转
  _link_page(url , type){

    //type  1 page跳转  2外链  3 tabbar
    if( type == 1){
      wx.navigateTo({
        url: url
      })
    }

    if( type == 2){
      
    }

    if( type == 3){
      wx.switchTab({
        url: url
      })
    }

  }

	

  /**
   * 判断是wx还是H5
   * author lgp
   */
  _isCloud(){
    if (typeof wx !== 'undefined' && typeof wx.getSystemInfoSync() !== 'undefined' ) {
      return 'wx';
    }
    else {
      return false;
    }
  }

};


export {
  Base
};
