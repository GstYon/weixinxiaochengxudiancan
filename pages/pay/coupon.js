//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

var coupon_id = 0;  //当前优惠券
var coupon_name = '';
Page({
  data: {
    coupon_guize: "<dd>仅限堂食</dd><dd>仅在微信点餐使用</dd><dd>优惠券不能与积分同享</dd><dd>嘉和一品部分门店可用，具体以门店活动公告为准</dd>",
    list:[],
    coupon_dotted:"" //优惠券灰色样式
  },
  onLoad: function () {
    
    var that = this;
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('order_coupon', function(data) {
      coupon_id = 0;
      coupon_name = '';
      
      console.log(data);
      that.setData({
        list: data.coupon
      })

      
    })

  },

  //优惠券选择变动
  radioChange(e) {
    console.log('radio发生change事件，携带value值为：', e)
    coupon_id = e.detail.value; 

    var data = this.data.list.use_list;
    for (var i in data){
      if (data[i]['coupon_id'] == e.detail.value){
        coupon_name = data[i]['title'];
        return false;
      }
    }
  },

  //选择优惠券
  submitCoupon(e){

    app.event.send('orderCouponChange', {
      coupon_id:coupon_id,
      coupon_name:coupon_name
    })
    wx.navigateBack();

  },





   // 详情弹框
   showModal(e) {
    
    this.setData({
      modalName: e.currentTarget.dataset.target,
      tabBarmodal:true,
      coupon_guize:e.currentTarget.dataset.content
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null,
      tabBarmodal:false
    })
  },
  
})
