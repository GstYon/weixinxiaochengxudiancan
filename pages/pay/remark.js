//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

var userInfo;

Page({
  data: {
    remark: ''
  },

  onLoad: function ( option ) {
    var that = this;
    
    console.log(option);
    this.setData(option)
    
  },
 
  //提交备注
  submitRemark: function(e) {
        
    app.event.send('order_remark', {
      remark:e.detail.value.remark
    })

    wx.navigateBack();
  }
})
