//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

var userInfo;
var store;
var order_mk_id = 0;
var pay_type = 1;  // 微信
var people_num = 0;// 餐具
var remark = '';   // 备注
var coupon_id = 0; // 优惠券ID
var couponInfo={}; // 优惠券内容
var __token__ = '';// 订单令牌
Page({
  data: {
    cart: {},
    remark:'',
    people_num_arr: [1,2,3,4,5,6,7,8,9,10],
    people_num_info:'',  //餐具费用描述
    table_num: 0
  },

  onLoad: function ( ) {
   
    base._showLoading( '加载中..');
    var that = this;
    const eventChannel = this.getOpenerEventChannel()
    eventChannel.on('cart', function(data) {
      
      that.setData({
        cart: data.cart
      })

      order_mk_id = data.order_mk_id;
      console.log(1111);
      //确认订单
      that.getorder();

      
    })
    //设置当前桌台信息
    this.setData({
      table_num: wx.getStorageSync('table_num')
    })

    //监听备注信息
    app.event.on('order_remark', this, function (ret) {
      console.log('备注：');
      console.log(ret);
      remark = ret.remark;
      that.setData({
        remark:remark
      })
    })

    //监听选择优惠券
    app.event.on('orderCouponChange', this, function (ret) {

      console.log('优惠券变动：');
      console.log(ret);

      coupon_id = ret.coupon_id;

      that.setData({
        coupon_name:ret.coupon_name
      })
      //确认订单
      that.getorder();
    })

    
    //监听用户登录信息
    app.event.on('userInfo', this, function (ret) {
      //确认订单
      that.getorder();
    })
    
    
  },
  //确认订单
  getorder:function(){
    userInfo = wx.getStorageSync('userInfo');
    if( !userInfo ){
        //未登录 跳转到 个人中心登录页
      return false;
    }
    
    store = wx.getStorageSync('store');

    var that = this;
    base._ajax(base.Conf.serverUrl + "v1/order/query", {
      token: userInfo.token, 
      store_id:store.id,
      order_mk_id:order_mk_id,
      goods:that.data.cart,
      coupon_id :coupon_id, //优惠券iD
    }, 'post', function (ret) {
     
      base._hideLoading();
        that.setData({
          order:ret.data
        })
        //订单令牌
        __token__ = ret.data.token;

    });


  },

  //订阅消息
  orderAndsubscribeMessage:function(){
  
    var that = this;
    //无论成功是否 都进行下单
    wx.requestSubscribeMessage({
      tmplIds: ['LuJgIqPKeauHPrx5kgrcu7RQvje0YdukOusV-2QD2T8' ,'cR2UDgmRSZ8Qf7tZ_UFfP7sH_A44V77rWtZ2KG0p4ao'],
      success (res) {
        that.order();
       },
       fail(err){
        that.order();
       }
    })

  },

  //提价订单
  order: function() {
    

    var that = this;
    let userInfo = wx.getStorageSync('userInfo');
    
    base._ajax(base.Conf.serverUrl + "v1/order/suborder", {
      token: userInfo.token, 
      store_id:store.id,
      order_mk_id:order_mk_id,
      goods:that.data.cart,
      pay_type:pay_type,  //支付方式
      people_num:people_num ,//餐具数量
      remark:remark, //备注
      coupon_id :coupon_id, //优惠券iD
      table_id: that.data.table_num,
      __token__:__token__
   }, 'post', function (ret) {
   
      if( ret.errcode == 0){
        //支付
        base._toast( '下单成功');
        // 支付方法

      } else if( ret.errcode == 310){
        //重复提交订单
        base._toast(ret.errmsg , function(){

        });
      } else {
        base._toast(ret.errmsg );
      }
   });
  },

  //跳转
  link_page:function(e){

    //获取当前list 索引
    var type = e.currentTarget.dataset.type;
    var url = e.currentTarget.dataset.url;

    base._link_page(url,type);

  },

  showModal(e) {
    
    this.setData({
      modalName: e.currentTarget.dataset.target,
      tabBarmodal:true
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null,
      tabBarmodal:false
    })
  },


  //选择餐具份数
  selectPeopleNum(e){
    people_num = e.currentTarget.dataset.num;
    let people_num_info = e.currentTarget.dataset.info;
    this.setData({
      people_num_info:people_num_info
    })
    this.hideModal();
  },


  //选择优惠券
  selectCoupin:function(e){

    let that = this;
    wx.navigateTo({
      url: "/pages/pay/coupon",
      success:function(res){
        res.eventChannel.emit('order_coupon', { coupon: that.data.order.coupon})
      }

    })

  },
})
