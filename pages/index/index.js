const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

Page({
  data: {
    PageCur: 'basics',
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    Custom: app.globalData.Custom,
    TabCur: 0,
    MainCur: 0,
    VerticalNavTop: 0,
    list: [],
    load: true,



    cardCur: -1, //默认变大选项
    swiperList: [{
      id: 0,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84000.jpg'
    }, {
      id: 1,
        type: 'image',
        url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84001.jpg',
    }, {
      id: 2,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big39000.jpg'
    }, {
      id: 3,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg'
    }, {
      id: 4,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big25011.jpg'
    }, {
      id: 5,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big21016.jpg'
    }, {
      id: 6,
      type: 'image',
      url: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big99008.jpg'
    }]
  },
  onLoad() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    });
  },
  onReady() {
    wx.hideLoading()
  },



  //扫码
  scanCode: function () {
    wx.scanCode({
      success: (res) => {
        console.log(res)
        var txt = res.result;
        var getUrlParam = function (t) {
          console.log(t);
          var data = t.split('/');
          return data[5];
        };
        var new_qrcode = txt.indexOf("weixin.qq.com");
        if (new_qrcode < 0){
          var d = txt.split('?');
          var e = d[1];
          var g = '?' + e;
          var a = getUrlParam(g);
        } else {
          var a = txt;
        }
        console.log(a);  //二维码参数的值
        //跳转到门店页面
        wx.navigateTo({
          url: '/pages/food/index?code=' + a
        })
      }
    })
  },

  closeMenu:function(e){
    base._toast('敬请期待...');

  },

  //跳转
  link_page:function(e){

    //获取当前list 索引
    var type = e.currentTarget.dataset.type;
    var url = e.currentTarget.dataset.url;

    base._link_page(url,type);

  }


})