//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

var coupon_type = 1; //1可用 2已使用 3已过期
Page({
  data: {
    TabCur: 1,
    scrollLeft:0,
    coupon_guize: "<dd>仅限堂食</dd><dd>仅在微信点餐使用</dd><dd>优惠券不能与积分同享</dd><dd>嘉和一品部分门店可用，具体以门店活动公告为准</dd>",
    list:[],
    coupon_dotted:"" //优惠券灰色样式
  },
  onLoad: function () {
    
    //获取列表
    this.getUserCoupon();
  },
  tabSelect(e) {
    var coupon_dotted = "";
    if( e.currentTarget.dataset.id != 1 ){
      coupon_dotted= "background:#ccc;";
    }
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id-1)*60,
      coupon_dotted:coupon_dotted
    })

    coupon_type = e.currentTarget.dataset.id;
    this.getUserCoupon();

  },


  //获取优惠券列表
  getUserCoupon: function() {
    
    var that = this;
    let userInfo = wx.getStorageSync('userInfo');
    base._ajax(base.Conf.serverUrl + "v1/user/coupon", {
      token: userInfo.token, 
      type:coupon_type
   }, 'post', function (ret) {
     if(ret.errcode ==0){
      that.setData({
        list: ret.data
      })
     } else if( ret.errcode==302) {
      //暂无优惠券
      that.setData({
        list: []
      })
     }
    

   });
  },



   // 详情弹框
   showModal(e) {
    
    this.setData({
      modalName: e.currentTarget.dataset.target,
      tabBarmodal:true,
      coupon_guize:e.currentTarget.dataset.content
    })
  },
  hideModal(e) {
    this.setData({
      modalName: null,
      tabBarmodal:false
    })
  },
  
})
