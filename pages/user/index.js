//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../utils/base.js';
var base = new Base();//初始化对象

var userInfo;
Page({
  data: {
    userInfo: {},   //微信用户信息及 token
    hasUserInfo: false,
    row: {
      total_balance:0,
      coupon_num:0,
      cre_balance:0
    }, //平台用户信息
  },

  //tarbar切换
  onTabItemTap(item) {
    console.log(item.pagePath)
    console.log(item.text)
    this.updateLoginStatus();
  },

  onLoad: function () {
    var that = this;
    //监听用户登录信息
    app.event.on('userInfo', this, function (ret) {
      //更新登录状态
      that.updateLoginStatus();
    })
    //更新登录状态
    that.updateLoginStatus();
    
  },
  //刷新登录状态
  updateLoginStatus:function(){
    userInfo = wx.getStorageSync('userInfo');
    if( userInfo ){
      this.setData({
        userInfo: userInfo,
        hasUserInfo: true
      })
      //获取用户信息
      this.getUserInfo();
    } else {
      this.setData({
        userInfo: {},
        row: {
          total_balance:0,
          coupon_num:0,
          cre_balance:0
        },
        hasUserInfo: false
      })
    }
  },

  /**
  * 弹出授权
  */
 bindGetUserInfo: function (e) {
    var that = this;
    app.wxlogin( function(ret){
      that.setData({
        userInfo: ret,
        hasUserInfo: true
      })
      console.log("login_call");
      console.log(ret);
      //登录成功获取用户信息
      that.getUserInfo();

    });
 },
  getUserInfo: function() {
    
    var that = this;
    let userInfo = this.data.userInfo;
    base._ajax(base.Conf.serverUrl + "v1/user/info", {
      token: userInfo.token, 
     
   }, 'post', function (ret) {
    that.setData({
      row: ret.data
    })

   });
  },


  //跳转
  link_page:function(e){

    //获取当前list 索引
    var type = e.currentTarget.dataset.type;
    var url = e.currentTarget.dataset.url;

    base._link_page(url,type);

  }
})
