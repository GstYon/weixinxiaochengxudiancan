//index.js
//获取应用实例
const app = getApp()

import { Base } from '../../../utils/base.js';
var base = new Base();//初始化对象

var userInfo;
var order_id = 0;
Page({
  data: {
     row:{}
  },

  onLoad: function ( option  ) {
   
    base._showLoading( '加载中..');
    var that = this;  

    order_id = option.order_id;
    
    this.getorder();
  },
  //获取订单信息
  getorder:function(){
    userInfo = wx.getStorageSync('userInfo');
    if( !userInfo ){
        //未登录 跳转到 个人中心登录页
      base._link_page("/pages/user/index",3);
      return false;
    }
    

    var that = this;
    base._ajax(base.Conf.serverUrl + "v1/order/detail", {
      token: userInfo.token, 
      order_id:order_id
    }, 'post', function (ret) {
      base._hideLoading();
      if(ret.errcode==0){
       
        that.setData({
          row:ret.data
        })
      } 
    });
  },

  //跳转
  link_page:function(e){

    //获取当前list 索引
    var type = e.currentTarget.dataset.type;
    var url = e.currentTarget.dataset.url;

    base._link_page(url,type);

  }
})
