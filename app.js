//app.js


var event = require('./utils/events.js');

import { Base } from './utils/base.js';
var base = new Base();//初始化对象

App({
  onLaunch: function () {
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []


      

  },
  globalData: {
    userInfo: null
  },
  event: event,

  //统一微信登录
  wxlogin:function(_call){
    //获取CODE信息
    wx.login({
      success(c) {
        //获取用户信息
        wx.getUserInfo({
          success: res => {
            console.log("getUserInfo");
            console.log(res);
            base._ajax(base.Conf.serverUrl + "v1/login", {
               data: res.encryptedData, 
              //  rawData:res.rawData,
               iv: res.iv,
               signature:res.signature,
               code:c.code
            }, 'post', function (ret) {
              
              let userInfo = JSON.parse(res.rawData);
              userInfo.token = ret.data.token;
              userInfo.open_id = ret.data.open_id;
              wx.setStorageSync('userInfo', userInfo);

              if (typeof _call == "function") {
                _call(userInfo);
              }
              
            });
          }
        }) 
      }
    });


  }
})